package repository

import (
	"gitlab.com/fannyhasbi/article-service/model"
)

// PageStorage store every page
type PageStorage struct {
	PageMap map[string]model.Page
}

// CreatePageStorage create a storage for page
func CreatePageStorage() *PageStorage {
	return &PageStorage{
		PageMap: make(map[string]model.Page),
	}
}

func (s *PageStorage) IsSlugExist(slug string) bool {
	for _, post := range s.PageMap {
		if post.Slug == slug {
			return true
		}
	}
	return false
}

// SavePage create a key and value into StorageMap
func (ps *PageStorage) SavePage(page *model.Page) string {
	ps.PageMap[page.Slug] = *page
	return page.Slug
}
