package model

import (
	"fmt"
	"strings"
	"time"
)

type Page struct {
	Slug      string
	Name      string
	Body      string
	CreatedAt time.Time
	Status    PageStatus
}

type PageService interface {
	IsPageLimited() bool
	CheckSlugIsExist(slug string) bool
}

type PageStatus string

const (
	PagePublished    PageStatus = "published"
	PageNotPublished PageStatus = "notpublished"
)

// CreatePage create an instance of Page
func CreatePage(ps PageService, name, slug, body string) (Page, error) {
	if ps.IsPageLimited() {
		return Page{}, fmt.Errorf("cannot create page over the limit")
	}

	if len(slug) > 0 {
		if ps.CheckSlugIsExist(slug) {
			return Page{}, fmt.Errorf("cannot create page with provided slug")
		}
	} else {
		temp := strings.Fields(slug)
		slug = strings.Join(temp, "-")
		timeStamp := time.Now().UnixNano()
		timeString := fmt.Sprintf("%d", timeStamp)
		slug += " - " + timeString
	}

	return Page{
		Name:      name,
		Slug:      slug,
		Body:      body,
		CreatedAt: time.Now(),
		Status:    PageNotPublished,
	}, nil
}
