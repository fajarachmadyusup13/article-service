package services

import (
	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
)

type ArticleServiceInMemory struct {
	Repository *repository.ArticleStorage
}

func NewArticleService(repo *repository.ArticleStorage) model.PostService {
	return &ArticleServiceInMemory{
		Repository: repo,
	}
}

func (a *ArticleServiceInMemory) CheckSlugIsExist(slug string) bool {
	exist := a.Repository.IsSlugExist(slug)
	return exist
}
