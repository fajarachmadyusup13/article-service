package services

import (
	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
)

const PageLimit = 3

// PageServiceInMemory for page service
type PageServiceInMemory struct {
	Repository *repository.PageStorage
}

// NewPageService create new service for Page
func NewPageService(repo *repository.PageStorage) model.PageService {
	return &PageServiceInMemory{
		Repository: repo,
	}
}

func (a *PageServiceInMemory) CheckSlugIsExist(slug string) bool {
	return a.Repository.IsSlugExist(slug)
}

// IsPageLimited check whether the page is more than the limit or not
func (ps *PageServiceInMemory) IsPageLimited() bool {
	if len(ps.Repository.PageMap) >= PageLimit {
		return true
	}

	return false
}
